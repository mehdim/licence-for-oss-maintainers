# Licence for OSS maintainers

A project of a trademark licence compatible with Open Source and Free Software licences, in order to get a better distribution of value for OSS maintainers. 
It has been initiated by [Mehdi Medjaoui](https://docs.google.com/document/d/1V0cefukZpxQDsyKo7JeTBeyee77J8PNF4_iviRfXG7k/edit#heading=h.59qvmrmu50s)

It encompasses: 
- A [Trademark licence](def_licence.md)
- A [Governance Framework](def_governance.md)